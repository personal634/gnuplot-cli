
# GNUPlot CLI

CLI program that takes user input, generates
GNUPlot scripts according to said input and
executes it.

## Usage

The program comes with a ``--help`` option which
prints the available CLI options:

```text
gnuplot_cli 1.0
XXXXXX X. <XXXXXXXXXXXXXXXXXXX@XXXXX.XXX>

USAGE:
    gnuplot_cli [OPTIONS] --input <input> <SUBCOMMAND>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -c, --columns <columns>
            CSV columns to plot (column index starts at 1). [default: 2]

    -d, --delimiter <delimiter>                        CSV column delimiter. [default: ;]
    -e, --extra-plot-opts <extra-plot-opts>
            Extra GnuPlot options during plot as JSON string (same options for all columns) or JSON
            dict (custom options for each plotting column). [default: {}]

    -i, --input <input>                                Input CSV.
    -j, --json-parameters <json-parameters>            Take CLI options from JSON file.
    -o, --output <output>                              Output plot filename. [default: out.png]
    -p, --pre-plot-extra-opts <pre-plot-extra-opts>
            Extra GnuPlot options before plotting as JSON list of strings. [default: []]

        --term-opts <term-opts>                        Extra GnuPlot terminal options. [default:  ]
    -t, --title <title>                                Plot title. [default: Plot]
    -x, --xlabel <xlabel>                              X axis name. [default: X]
        --xlim <xlim>
            X axis limits as two comma-separated real values.

    -y, --ylabel <ylabel>                              Y axis name. [default: Y]
        --ylim <ylim>
            Y axis limits as two comma-separated real values.


SUBCOMMANDS:
    help       Prints this message or the help of the given subcommand(s)
    plot       Line plot.
    scatter    Scatter plot.
```

## Plot example

An example is located at ``examples`` directory,
which contains the input files ``example.dat``
(the plot data), and ``example.json`` (the extra
CLI arguments).

In order to create the plot, type the following
command:

```bash
$ gnuplot_cli -j example.json --input example.dat plot
```

which yields a GNUPlot script ``test.gp``, a LaTeX
standalone script ``example.tex`` and the EPS plot
``example.eps``. Note that some additional files
are generated: ``fit.log`` contains GNUPlot
information about curve fitting parameters used
on example and ``texput.log`` with LaTeX compile
information.

![alt](examples/example.png)