//! Read GnuPlot options from JSON file.

use std::path::Path;
use std::fs::File;
use crate::gen_gp::{JsonCommands, JsonExtraCmd, JsonPreCmd};
use std::io::Read;
use serde_json::Value;

pub fn from_json_file(filename: &Path) -> Result<JsonCommands, i8> {
    //! Get optional parameters from JSON ``filename``.
    //! In Err case, return file open/read error (2) or parse error (4).

    let mut f: File = File::open(filename)
        .map_err(|e| {
            println!("Error opening file: {}.", e.to_string());
            2
    })?;
    let mut content: String = String::from("");
    f.read_to_string(&mut content)
        .map_err(|e| {
            println!("Error reading file: {}.", e.to_string());
            2
        })?;
    let content_str: &str = content.as_str();
    // println!("Message:\n{}", content_str);
    /*
    let deserialized: JsonCommands = serde_json::from_str(content_str)
        .map_err(|e| {
            println!("Error parsing JSON: {}.", e.to_string());
            4
        })?;
     */
    let deserialized: Result<JsonCommands, i8> = serde_json::from_str(content_str)
        .map_err(|e| {
            println!("Warning parsing JSON (missing files?): {}.", e.to_string());
            4
        });

    return match deserialized {
        Ok(j) => Ok(j),
        Err(_) => {
            let json_opts: Value = serde_json::from_str(content_str)
                .map_err(|e| {
                    println!("Error parsing JSON: {}.", e.to_string());
                    4
                })?;
            let mut opts: JsonCommands = JsonCommands {
                term: Value::Null,
                input: Value::Null,
                delimiter: Value::Null,
                plot_columns: Value::Null,
                title: Value::Null,
                xlabel: Value::Null,
                ylabel: Value::Null,
                xlim: Value::Null,
                ylim: Value::Null,
                output: Value::Null,
                term_opts: Value::Null,
                extra_plot_opts: Value::Null,
                pre_plot_opts: Value::Null
            };

            if let Value::Object(h) = json_opts {
                opts.term = h.get("term").map(|o| o.clone()).unwrap_or(Value::Null);
                opts.input = h.get("input").map(|o| o.clone()).unwrap_or(Value::Null);
                opts.delimiter = h.get("delimiter").map(|o| o.clone()).unwrap_or(Value::Null);
                opts.plot_columns = h.get("plot_columns").map(|o| o.clone()).unwrap_or(Value::Null);
                opts.title = h.get("title").map(|o| o.clone()).unwrap_or(Value::Null);
                opts.xlabel = h.get("xlabel").map(|o| o.clone()).unwrap_or(Value::Null);
                opts.ylabel = h.get("ylabel").map(|o| o.clone()).unwrap_or(Value::Null);
                opts.xlim = h.get("xlim").map(|o| o.clone()).unwrap_or(Value::Null);
                opts.ylim = h.get("ylim").map(|o| o.clone()).unwrap_or(Value::Null);
                opts.output = h.get("output").map(|o| o.clone()).unwrap_or(Value::Null);
                opts.term_opts = h.get("term_opts").map(|o| o.clone()).unwrap_or(Value::Null);
                opts.extra_plot_opts = h.get("extra_plot_opts").map(|o| o.clone()).unwrap_or(Value::Null);
                opts.pre_plot_opts = h.get("pre_plot_opts").map(|o| o.clone()).unwrap_or(Value::Null);
            }

            Ok(opts)
        }
    }
}

pub fn from_json_pre_plot(val: &String) -> Result<JsonPreCmd, i8> {
    //! Parse a subset of custom options from JSON notation. In case of parsing error, return 4.
    let deserialized: JsonPreCmd = serde_json::from_str(val.as_str())
        .map_err(|e| {
            println!("Error parsing pre-plot data from JSON: {}.", e.to_string());
            4
        })?;
    Ok(deserialized)
}

pub fn from_json_extra_plot(val: &String) -> Result<JsonExtraCmd, i8> {
    //! Parse a subset of custom plotting options from JSON notation. In case of parsing error, return 4.
    let deserialized: JsonExtraCmd = serde_json::from_str(val.as_str())
        .map_err(|e| {
            println!("Error parsing extra plot data from JSON: {}.", e.to_string());
            4
        })?;
    Ok(deserialized)
}