/// Generate GnuPlot script.

use std::path::Path;
use std::fs::File;
use std::io::Write;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;

#[derive(Serialize, Deserialize)]
pub struct JsonPreCmd {
    pub pre_plot_opts : Value
}

#[derive(Serialize, Deserialize)]
pub struct JsonExtraCmd {
    pub extra_plot_opts : Value
}

#[derive(Serialize, Deserialize)]
pub enum JsonCommandsExtraPlot {
    Single (String),
    Multi (HashMap<String, String>)
}

#[derive(Serialize, Deserialize)]
pub struct JsonCommands {
    pub term: Value,
    pub input: Value,
    pub delimiter: Value,
    pub plot_columns: Value,
    pub title: Value,
    pub xlabel: Value,
    pub ylabel: Value,
    pub xlim: Value,
    pub ylim: Value,
    pub output: Value,
    pub term_opts: Value,
    pub extra_plot_opts: Value,
    pub pre_plot_opts: Value
}

pub struct GnuplotCommands<'a> {
    pub term: String,
    pub input: &'a Path,
    pub delimiter: String,
    pub plot_columns: Vec<usize>,
    pub title: String,
    pub xlabel: String,
    pub ylabel: String,
    pub xlim: Option<[f32; 2]>,
    pub ylim: Option<[f32; 2]>,
    pub output: String,
    pub plot_type: PlotType,
    pub pre_plot_opts: Vec<String>,
    pub extra_plot_opts: HashMap<usize, String>,
    pub term_opts: String,
}

pub enum PlotType {
    PLOT,
    SCATTER
}

pub fn gen_gp(filename: &Path, g_ops: GnuplotCommands) -> Result<(), i8> {
    //! Generate GnuPlot script ``filename`` and fill it with ``g_ops`` GnuPlot options.
    //! In case of error, return file open/read/write error (2) or type casting error (3).
    let mut f = File::create(filename)
        .map_err(|e| {
            println!("Error opening file: {}.", e.to_string());
            2
        })?;

    f.write_fmt(format_args!("set term {} {}\n", g_ops.term, g_ops.term_opts))
        .map_err(|e| {
            println!("Error recording output terminal: {}.", e.to_string());
            2
        })?;
    f.write_fmt(format_args!("set output '{}'\n", g_ops.output))
        .map_err(|e| {
            println!("Error recording output: {}.", e.to_string());
            2
        })?;
    f.write_fmt(format_args!("set datafile separator '{}'\n", g_ops.delimiter))
        .map_err(|e| {
            println!("Error recording data delimiter: {}.", e.to_string());
            2
        })?;
    f.write_fmt(format_args!("set title '{}'\n", g_ops.title))
        .map_err(|e| {
            println!("Error recording title: {}.", e.to_string());
            2
        })?;
    f.write_fmt(format_args!("set xlabel '{}'\n", g_ops.xlabel))
        .map_err(|e| {
            println!("Error recording xlabel: {}.", e.to_string());
            2
        })?;
    f.write_fmt(format_args!("set ylabel '{}'\n", g_ops.ylabel))
        .map_err(|e| {
            println!("Error recording ylabel: {}.", e.to_string());
            2
        })?;

    if g_ops.xlim.is_some() {
        let rx = g_ops.xlim.unwrap();
        f.write_fmt(format_args!("set xrange[{}:{}]\n", rx[0], rx[1]))
            .map_err(|e| {
                println!("Error recording xrange: {}.", e.to_string());
                2
            })?;
    }

    if g_ops.ylim.is_some() {
        let ry = g_ops.ylim.unwrap();
        f.write_fmt(format_args!("set yrange[{}:{}]\n", ry[0], ry[1]))
            .map_err(|e| {
                println!("Error recording yrange: {}.", e.to_string());
                2
            })?;
    }

    for command in g_ops.pre_plot_opts {
        f.write_fmt(format_args!("{}\n", command))
            .map_err(|e| {
                println!("Error recording GnuPlot command: {}.", e.to_string());
                2
            })?;
    }

    // println!("{}", g_ops.input.to_str().unwrap());
    let inp: &str = match g_ops.input.to_str() {
        Some(s) => s,
        None => {
            println!("Error casting input filename into str.");
            return Err(3);
        }
    };

    let l_opts: &str = match g_ops.plot_type {
        PlotType::PLOT => "w l",
        PlotType::SCATTER => ""
    };

    for (i, c) in g_ops.plot_columns.iter().enumerate() {
        let def_val: String = String::from("");
        let extra_plot_opts: &String = g_ops.extra_plot_opts.get(c)
            .unwrap_or(&def_val);
        if i == 0 {
            if i == g_ops.plot_columns.len() - 1 {
                f.write_fmt(format_args!(
                    "plot '{}' u 1:{} {} title 'Y{}' {}\n",
                    inp,
                    c,
                    l_opts,
                    c-1,
                    extra_plot_opts
                ))
                    .map_err(|e| {
                        println!("Error recording plot options: {}.", e.to_string());
                        2
                    })?;
            }
            else {
                f.write_fmt(format_args!(
                    "plot '{}' u 1:{} {} title 'Y{}' {},\\\n",
                    inp,
                    c,
                    l_opts,
                    c-1,
                    extra_plot_opts
                ))
                    .map_err(|e| {
                        println!("Error recording plot options: {}.", e.to_string());
                        2
                    })?;
            }
        }
        else {
            if i == g_ops.plot_columns.len() - 1 {
                f.write_fmt(format_args!(
                    "'' u 1:{} {} title 'Y{}' {}\n",
                    c,
                    l_opts,
                    c-1,
                    extra_plot_opts
                ))
                    .map_err(|e| {
                        println!("Error recording plot options: {}.", e.to_string());
                        2
                    })?;
            }
            else {
                f.write_fmt(format_args!(
                    "'' u 1:{} {} title 'Y{}' {},\\\n",
                    c,
                    l_opts,
                    c-1,
                    extra_plot_opts
                ))
                    .map_err(|e| {
                        println!("Error recording plot options: {}.", e.to_string());
                        2
                    })?;
            }
        }
    }
    Ok(())
}
