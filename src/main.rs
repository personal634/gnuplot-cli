/// Generate GnuPlot plots from user input.

mod gen_gp;
mod parse_json;

// use std::env::temp_dir;
use std::process::Command;
use clap::Clap;
use std::path::Path;
use std::iter::FromIterator;
use gen_gp::{GnuplotCommands, JsonCommands, PlotType, gen_gp};
use parse_json::from_json_file;
use serde_json::Value;
use std::collections::HashMap;

#[derive(Clap)]
#[clap(version="1.0", author="Imanol S. <sardondelgadoimanol@gmail.com>")]
struct Opts {
    #[clap(subcommand)]
    subcmd: SubCommand,
    #[clap(short, long, about="Input CSV.")]
    input: String,
    #[clap(short, long, about="Take CLI options from JSON file.")]
    json_parameters: Option<String>,
    #[clap(short, long, default_value=";", about="CSV column delimiter.")]
    delimiter: String,
    #[clap(short, long, default_value="2", about="CSV columns to plot (column index starts at 1).")]
    columns: String,
    #[clap(short, long, default_value="Plot", about="Plot title.")]
    title: String,
    #[clap(short, long, default_value="X", about="X axis name.")]
    xlabel: String,
    #[clap(short, long, default_value="Y", about="Y axis name.")]
    ylabel: String,
    #[clap(long, about="X axis limits as two comma-separated real values.")]
    xlim: Option<String>,
    #[clap(long, about="Y axis limits as two comma-separated real values.")]
    ylim: Option<String>,
    #[clap(short, long, default_value="out.png", about="Output plot filename.")]
    output: String,
    #[clap(short, long, default_value="[]", about="Extra GnuPlot options before plotting as JSON list of strings.")]
    pre_plot_extra_opts : String,
    #[clap(short, long, default_value="{}", about="Extra GnuPlot options during plot as JSON string (same options for all columns) or JSON dict (custom options for each plotting column).")]
    extra_plot_opts : String,
    #[clap(long, default_value=" ", about="Extra GnuPlot terminal options.")]
    term_opts : String,
}

#[derive(Clap)]
enum SubCommand {
    #[clap(version = "1.0", author = "Imanol S. <sardondelgadoimanol@gmail.com>", about="Scatter plot.")]
    Scatter,
    #[clap(version = "1.0", author = "Imanol S. <sardondelgadoimanol@gmail.com>", about="Line plot.")]
    Plot
}


fn main() -> Result<(), i8> {
    let mut opts: Opts = Opts::parse();

    // Replace CLI missing field defaults with JSON args.
    let mut json_term: Value = Value::Null;
    if let Some(json_input_string) = &opts.json_parameters {
        let json_input: &Path = Path::new(&json_input_string);
        match from_json_file(json_input) {
            Ok(val) => {
                json_term = val.term.clone();
                defaults_from_json(&mut opts, val);
            },
            Err(e) => {
                println!("Warning: could not get parameters from JSON file: Error Number {}.", e);
            },
        };
    }

    let input: &Path = Path::new(&opts.input);
    if ! input.exists() {
        println!("Input data file does not exist.");
        return Err(1);
    }

    let extension: &str = match Path::new(&opts.output).extension() {
        None => {
            println!("Unknown filename extension: {}", opts.output);
            return Err(1);
        },
        Some(ext) => match ext.to_str() {
            None => {
                println!("Could not convert extension into str.");
                return Err(1);
            },
            Some(ext_str) => ext_str,
        }
    };

    let term_opt: Option<&str> = match extension.to_lowercase().as_str() {
        "png" => Some("pngcairo"),
        "svg" => Some("svg"),
        "ps" => Some("postscript"),
        "eps" => Some("postscript"),
        "tex" => Some("epslatex"),
        _ => None
    };

    let mut term = match term_opt {
        Some(t_str) => String::from(t_str),
        None => {
            println!("E: Unknown terminal.");
            return Err(1);
        }
    };
    if let Value::String(s) = json_term { term = s; }

    let term_opts: String = opts.term_opts;

    let delimiter: String = opts.delimiter;

    let plot_columns: Vec<usize> = Vec::from_iter(opts.columns.split(',')
        .map(|s| String::from(s).parse::<usize>().unwrap())
    );

    let title: String = opts.title;
    let xlabel: String = opts.xlabel;
    let ylabel: String = opts.ylabel;

    let xlim: Option<[f32; 2]> = match_lim(opts.xlim);
    let ylim: Option<[f32; 2]> = match_lim(opts.ylim);

    let output: String = opts.output;

    let mut pre_plot_opts: Vec<String> = Vec::new();
    if let Ok(r) = parse_json::from_json_pre_plot(
        &format!("{{ \"pre_plot_opts\": {} }}", opts.pre_plot_extra_opts)
    ) {
        match r.pre_plot_opts {
            Value::String(s) => pre_plot_opts.push(s),
            Value::Array(arr) => {
                for vv in arr {
                    if let Value::String(vvv) = vv { pre_plot_opts.push(vvv); }
                }
            },
            _ => {},
        }
    }

    let mut extra_plot_opts: HashMap<usize, String> = HashMap::new();
    if let Ok(r) = parse_json::from_json_extra_plot(
        &format!("{{ \"extra_plot_opts\" : {} }}", &opts.extra_plot_opts)
    ) {
        match r.extra_plot_opts {
            Value::String(s) => {
                for c in &plot_columns {
                    extra_plot_opts.insert(c.clone(), s.clone());
                }
            },
            Value::Object(h) => {
                for (k, v) in h {
                    if let (Ok(k_int), Value::String(ss)) = (k.parse::<usize>(), v) {
                        extra_plot_opts.insert(k_int, ss);
                    }
                }
            },
            _ => {},
        }
    }


    let plot_type: PlotType = match opts.subcmd {
        SubCommand::Scatter => PlotType::SCATTER,
        SubCommand::Plot => PlotType::PLOT
    };

    let g_opts: GnuplotCommands = GnuplotCommands {
        term: String::from(term.as_str()),
        input,
        delimiter,
        plot_columns,
        title,
        xlabel,
        ylabel,
        xlim,
        ylim,
        output: String::from(output.as_str()),
        plot_type,
        pre_plot_opts,
        extra_plot_opts,
        term_opts
    };

    // let temp_filename: &Path = temp_dir().join("temp.gp").as_path();
    // gen_gp(temp_filename, g_opts)
    let temp_filename: &Path = Path::new("test.gp");
    gen_gp(temp_filename, g_opts)?;
    let _output = Command::new("gnuplot")
        .arg(temp_filename)
        .output()
        .map_err(|e| {
            println!("Error executing GnuPlot: {}.", e.to_string());
            5
        })?;
    if term.to_lowercase() == "epslatex" {
        let output_name: String = match (output.clone()).rfind('.') {
            None => output,
            Some(n) => String::from(output.split_at(n).0),
        };
        Command::new("latex")
            .args(&[
                String::from("-interaction=nonstopmode"),
                format!("{}.tex", &output_name)
            ])
            .output()
            .map_err(|e| {
                println!("Error executing Latex: {}.", e.to_string());
                5
            })?;
        Command::new("dvips")
            .args(&[
                String::from("-o"),
                format!("{}.ps", &output_name),
                format!("{}.dvi", &output_name)
            ])
            .output()
            .map_err(|e| {
                println!("Error executing DVIPS: {}.", e.to_string());
                5
            })?;
        Command::new("ps2eps")
            .args(&[
                String::from("-f"),
                format!("{}.ps", &output_name),
            ])
            .output()
            .map_err(|e| {
                println!("Error executing PS2EPS: {}.", e.to_string());
                5
            })?;

        Command::new("rm")
            .args(&[
                String::from("-f"),
                format!("{}.ps", &output_name),
                format!("{}.dvi", &output_name),
                format!("{}-inc.eps", &output_name),
                format!("{}.log", &output_name),
                format!("{}.aux", &output_name),
            ])
            .output()
            .map_err(|e| {
                println!("Error executing RM: {}.", e.to_string());
                5
            })?;
    }
    Ok(())
}

fn match_lim(lim: Option<String>) -> Option<[f32; 2]> {
    //! Deserialize string into X or Y limits, if possible, else return None.
    return match lim {
        Some(s) => {
            let mut values: Vec<f32> = Vec::new();
            for s in Vec::from_iter(s.split(',')) {
                if let Ok(v) = String::from(s).parse::<f32>() { values.push(v); }
            }
            if values.len() == 2 {
                return Some([values[0], values[1]]);
            }
            return None;
        },
        None => None,
    };
}

fn defaults_from_json(opts: &mut Opts, val: JsonCommands) {
    //! Override default GnuPlot options with user options taken from a provided JSON file.
    // Override delimiter if custom delimiter was specified in JSON
    // and CLI delimiter was default (otherwise user did specify another
    // custom delimiter on CLI, which takes precedence).
    if let Value::String(s) = val.delimiter {
        if opts.delimiter == ";" { opts.delimiter = s; }
    }

    // Override terminal options.
    if let Value::String(s) = val.term_opts {
        if opts.term_opts == " " { opts.term_opts = s; }
    }

    // Override output
    if let Value::String(s) = val.output {
        if opts.output == "out.png" { opts.output = s; }
    }

    // Override plot title.
    if let Value::String(s) = val.title {
        if opts.title == "Plot" { opts.title = s; }
    }

    // Override labels.
    if let Value::String(s) = val.xlabel {
        if opts.xlabel == "X" { opts.xlabel = s; }
    }
    if let Value::String(s) = val.ylabel {
        if opts.ylabel == "Y" { opts.ylabel = s; }
    }

    // Override limits.
    if let Value::Array(arr) = val.xlim {
        if arr.len() == 2 {
            if let (Value::Number(v1), Value::Number(v2)) = (&arr[0], &arr[1]) {
                if let (Some(vv1), Some(vv2)) = (v1.as_f64(), v2.as_f64()) {
                    opts.xlim = Some(format!("{}, {}", vv1 as f32, vv2 as f32));
                }
            }
        }
    }
    if let Value::Array(arr) = val.ylim {
        if arr.len() == 2 {
            if let (Value::Number(v1), Value::Number(v2)) = (&arr[0], &arr[1]) {
                if let (Some(vv1), Some(vv2)) = (v1.as_f64(), v2.as_f64()) {
                    opts.ylim = Some(format!("{}, {}", vv1 as f32, vv2 as f32));
                }
            }
        }
    }

    match val.extra_plot_opts {
        Value::Null => {},
        Value::String(s) => {
            if opts.extra_plot_opts != "{}" {
                opts.extra_plot_opts = s;
            }
        },
        Value::Object(hash) => {
            let mut new_ss : Vec<String> = Vec::new();
            for (k, v) in hash {
                if let (Ok(k_int), Value::String(s)) = (k.parse::<usize>(), v) {
                    new_ss.push(format!("\"{}\":\"{}\"", k_int, s));
                }
            }
            if opts.extra_plot_opts == "{}" {
                opts.extra_plot_opts = format!("{{ {} }}", new_ss.join(","));
            }
        }
        _ => println!("Warning: invalid 'extra_plot_opts' field on JSON file."),
    }

    match val.pre_plot_opts {
        Value::Null => {},
        Value::String(s) => opts.pre_plot_extra_opts = format!("[{}]", s),
        Value::Array(arr) => {
            let mut new_arr : Vec<String> = Vec::new();
            for v in arr {
                if let Value::String(s) = v {
                    new_arr.push(format!("\"{}\"", s));
                }
            }
            if opts.pre_plot_extra_opts == "[]" {
                opts.pre_plot_extra_opts = format!("[ {} ]", new_arr.join(","));
            }
        }
        _ => println!("Warning: invalid 'extra_plot_opts' field on JSON file."),
    }

}