set term epslatex size 3.5,2.62 standalone
set output 'example.tex'
set datafile separator ';'
set title 'Fit Models'
set xlabel 'X-axis'
set ylabel 'Y-axis'
set grid
set boxwidth
set key top left
f(x) = a0 + a1*x
g(x) = a0 + a1*x + a2*x**2 + a3*x**3
h(x) = a0 + a1*x + a2*x**2 + a3*x**3 + a4*x**4 + a5*x**5
FIT_LIMIT = 1e-9
fit f(x) 'example.dat' using 1:2 via a0,a1
fit g(x) 'example.dat' using 1:2 via a0,a1,a2,a3
fit h(x) 'example.dat' using 1:2 via a0,a1,a2,a3,a4,a5
plot 'example.dat' every 10 using 1:2 title 'Raw data', f(x) w l lw 2 dt 1 lc 2 title 'Line Fit', g(x) w l lw 2 dt 3 lc 6 title '$\mathbb{P}_3(x)$ Fit', h(x) w l lw 2 dt 5 lc 5 title '$\mathbb{P}_5(x)$ Fit'
set output
plot 'example.dat' u 1:2 w l title 'Y1'   
